# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!(name:  "Jonathan Higgins",
             email: "admin@jonathanhiggins.ie",
             password:              "password",
             password_confirmation: "password",
             activated: true,
             activated_at: Time.zone.now,
             admin: true)

User.create!(name:  "Brendan Zielger",
             email: "brendan.ziegler@student.ncirl.ie",
             password:              "password",
             password_confirmation: "password",
             activated: true,
             activated_at: Time.zone.now,
             admin: true)

 99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end
               
Product.create(title: 'Grey Casual Suit',
  description:
    %{<p>
        Grey Casual suit perfect for any ocassion.
      </p>},
  image_url: 'suit2.jpg',
  price: 200.00)

Product.create(title: 'Blue Wedding Suit',
  description:
    %{<p>
        Blue Wedding Suit perfect for your special day.
      </p>},
  image_url: 'suit1.jpg',
  price: 180.00)  
  
Product.create(title: 'Business Grey Suit',
  description:
    %{<p>
        Business Grey Suit.
      </p>},
  image_url: 'suit3.jpg',
  price: 175.00)  

Product.create(title: 'Casual Suit',
  description:
    %{<p>
        Casual Suit for all ocassions.
      </p>},
  image_url: 'suit4.jpg',
  price: 300.00)  
  
Product.create(title: 'Casual Red Shirt',
  description:
    %{<p>
        Casual red shirt.
      </p>},
  image_url: 'shirt1.jpg',
  price: 70.00)  
  
Product.create(title: 'Business Blue Shirt',
  description:
    %{<p>
        Business Blue Shirt.
      </p>},
  image_url: 'shirt2.jpg',
  price: 85.00)  
  
Product.create(title: 'White Shirt',
  description:
    %{<p>
        White Shirt.
      </p>},
  image_url: 'shirt3.jpg',
  price: 55.00)  
  
Product.create(title: 'Casual Shirt',
  description:
    %{<p>
        Casual shirt.
      </p>},
  image_url: 'suit4.jpg',
  price: 65.00)  
  
Product.create(title: 'Business Pants',
  description:
    %{<p>
        Business Pants.
      </p>},
  image_url: 'pants1.jpg',
  price: 90.00)  
  
Product.create(title: 'Sport Pants',
  description:
    %{<p>
        Sport pants.
      </p>},
  image_url: 'pants2.jpg',
  price: 110.00) 
  
Product.create(title: 'Casual Pants',
  description:
    %{<p>
        Casual Pants.
      </p>},
  image_url: 'pants3.jpg',
  price: 75.00) 
  
Product.create(title: 'Business Pants',
  description:
    %{<p>
        Business Pants.
      </p>},
  image_url: 'pants4.jpg',
  price: 60.00) 
  
Product.create(title: 'Business Shoes Brown',
  description:
    %{<p>
        Business Shoes.
      </p>},
  image_url: 'shoes1.jpg',
  price: 140.00)
  
Product.create(title: 'Business Shoes',
  description:
    %{<p>
        Business Shoes available in sizes 7-10.
      </p>},
  image_url: 'shoes2.jpg',
  price: 110.00)
  
Product.create(title: 'Upmarket Business shoes',
  description:
    %{<p>
        Business Pants.
      </p>},
  image_url: 'shoes3.jpg',
  price: 130.00)
  
Product.create(title: 'Sport shoes',
  description:
    %{<p>
        Sport shoes.
      </p>},
  image_url: 'shoes4.jpg',
  price: 100.00)

