
// form functions // 
// This is the function
	function getOnFocus(inputid)
	{
		document.getElementById(inputid).style.backgroundColor = "#C5CFC6";
	}
	
	function getOnBlur(inputid)
	{
		document.getElementById(inputid).style.backgroundColor = "white";
	}
	
	// This is the event OnFocus
	document.getElementById('emailone').onfocus = function() {
		getOnFocus("emailone");
	}
	
	document.getElementById('passwordone').onfocus = function() {
		getOnFocus("passwordone");
	}
	
	document.getElementById('fullname').onfocus = function() {
		getOnFocus("fullname");
	}
	
	document.getElementById('emailtwo').onfocus = function() {
		getOnFocus("emailtwo");
	}
	
	document.getElementById('confirmEmail').onfocus = function() {
		getOnFocus("confirmEmail");
	}
	
	document.getElementById('Password').onfocus = function() {
		getOnFocus("Password");
	}
	
	document.getElementById('confirmPassword').onfocus = function() {
		getOnFocus("confirmPassword");
	}
	
	// This is the event OnBlur
	
	document.getElementById('emailone').onblur = function() {
		getOnBlur("emailone");
	}
	
	document.getElementById('passwordone').onblur = function() {
		getOnBlur("passwordone");
	}
	
	document.getElementById('fullname').onblur = function() {
		getOnBlur("fullname");
	}
	
	document.getElementById('emailtwo').onblur = function() {
		getOnBlur("emailtwo");
	}
	
	document.getElementById('confirmEmail').onblur = function() {
		getOnBlur("confirmEmail");
	}
	
	document.getElementById('Password').onblur = function() {
		getOnBlur("Password");
	}
	
	document.getElementById('confirmPassword').onblur = function() {
		getOnBlur("confirmPassword");
	}
	
function validate()
      {
      
         if( document.signUp.fullname.value == "" )
         {
            alert( "Please provide your name!" );
            document.signUp.fullname.focus() ;
            return false;
         }
         
         if( document.signUp.email.value == "" )
         {
            alert( "Please provide your Email!" );
            document.myForm.email.focus() ;
            return false;
         }
         
         if( document.signUp.password.value == "" ||
         isNaN( document.signUp.password.value ) ||
         document.signUp.password.value.length < 6 )
         {
            alert( "Please enter a password more than 6 characters." );
            document.signUp.password.focus() ;
            return false;
         }
         
          if ($("#emailtwo").val() != $("#confirmEmail").val()) {
          	       alert("Emails do not match.");
      }
         
         if ($("#Password").val() != $("#confirmPassword").val()) {
          alert("Passwords do not match.");
      }
         
      }
      
       
$(document).ready(function() {
        //If Javascript is running, change css on product-description to display:block
        //then hide the div, ready to animate
        $("div.pop-up").css({'display':'block','opacity':'0'})

        $("a.trigger").hover(
          function () {
            $(this).prev().stop().animate({
              opacity: 1
            }, 500);
          },
          function () {
            $(this).prev().stop().animate({
              opacity: 0
            }, 200);
          }
        )
      });
    