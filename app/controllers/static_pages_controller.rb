class StaticPagesController < ApplicationController
  def home
  end

  def index
    render :layout => '_index.html.erb'
  end

  def contact
  end

  def about
  end
end
